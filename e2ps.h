/*=============================================================================
		e2ps.h
			by Nobuyuki SHIRAKI
			Last change : Wed 21 August 2002  17:51:05
=============================================================================*/


#ifndef		WT_E2PS_H
#define		WT_E2PS_H

#include	<stdio.h>
#include	<stdlib.h>
#include	<time.h>
#include	<signal.h>
#include	<string.h>
#include	<math.h>

/* System Information */
#define	NAME			"e2ps"
#define	LPRNAME			"e2lpr"
#define VERSION			(434)
#define DATE			"08/21/2002"
#define	CREATOR			"Nobuyuki Shiraki"
#define COPYRIGHTYEAR		"1999-2002"

/* Character Code */
#define BACKSPACE		(0x08)			/* BS */
#define TAB			(0x09)			/* TAB */
#define NEWLINE			(0x0a)			/* CR */
#define NEWPAGE			(0x0c)			/* NP */
#define	ESC			(0x1b)			/* Escape */

/* Paper */
#define A4WIDTH			(594)
#define	A4HEIGHT		(840)
#define LEWIDTH			(612)
#define LEHEIGHT		(796)
#define B4WIDTH			(730)
#define	B4HEIGHT		(1030)

/* sqrt(0.5) */
#define	SQRT			(0.707106781186548)

/* ASCII Fonts */
#ifndef	ASCII
#define	ASCII			"Courier"
#endif
#ifndef	ASCII_BOLD
#define	ASCII_BOLD		"Courier-Bold"
#endif
#ifndef	ASCII_BOLDITALIC
#define	ASCII_BOLDITALIC	"Courier-BoldOblique"
#endif

/* KANJI Fonts */
#define	KANJI			"Ryumin-Light-H"
#define	EUC_KANJI		"Ryumin-Light-EUC-H"
#define	KANJI_BOLD		"GothicBBB-Medium-H"
#define	EUC_KANJI_BOLD		"GothicBBB-Medium-EUC-H"
#define	KANJI_BOLDITALIC	"GothicBBB-Medium-H-Italic"
#define	EUC_KANJI_BOLDITALIC	"GothicBBB-Medium-EUC-H-Italic"

/* Font Size */
#ifndef	FONTSIZE
#define	FONTSIZE		(10.0)
#endif
#ifndef	OHPFONTSIZE
#define	OHPFONTSIZE		(25.0)
#endif
#ifndef	ASCIIWIDTH
#define	ASCIIWIDTH		(0.6)
#endif
#ifndef	ASCIIHEIGHT
#define	ASCIIHEIGHT		(1.0)
#endif
#ifndef	KANJIWIDTH
#define	KANJIWIDTH		(1.2)
#endif
#ifndef	KANJIHEIGHT
#define	KANJIHEIGHT		(1.0)
#endif

/* TAB Width */
#ifndef	TABSTOP
#define TABSTOP			(8)
#endif

/* Space ratio between lines */
#ifndef	NLRATE
#define	NLRATE			(1.1)
#endif

/* Paper Setting */
#ifndef	TOP
#define TOP			(40.0)			/* Top margin */
#endif
#ifndef	BOTTOM
#define BOTTOM			(40.0)			/* Bottom margin */
#endif
#ifndef	LEFT
#define LEFT			(40.0)			/* Left margin */
#endif
#ifndef	RIGHT
#define RIGHT			(40.0)			/* Right margin */
#endif
#ifndef	MAXLINE
#define MAXLINE			(66)			/* Lines */
#endif

/* Y2K */
#ifndef	Y2K
#define	Y2K			(0)
#endif

/* String size */
#define	STRMAX			(1024)

/* Typedef */
typedef	unsigned char	uchar;

/* Functions */
extern	char	*ps_font(char *,float *);
extern	int	check_kanjicode(uchar *);
extern	void	change_jis2euc(uchar *, uchar *);
extern	void	change_sjis2euc(uchar *, uchar *);
extern	void	print(uchar, char *);
extern	void	newline(uchar, char *, char *);
extern	void	newpage(char *);
extern	void	print_font(char *);
extern	void	print_header(char *, char *, char *, float, float, uchar);
extern	void	print_header_size(uchar *, char *, char *);
extern	void	print_header_string(uchar *, char *, char *);
extern	char	*escape(char);
extern	char	*setstring(char *);
extern	char	*setstringsize(int);
extern	char	*addstring(char *, char *);
extern	void	quit(int);
extern	void	help(char *);

/* Global variables */
extern	uchar	guJapanese;


#endif		/* WT_E2PS_H */


/*=============================================================================
			end
=============================================================================*/
