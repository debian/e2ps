/*=============================================================================
		wtkf.h
			by Nobuyuki SHIRAKI
			Last change : Tue  5 February 2002   0:38:12
=============================================================================*/


/*-----------------------------------------------------------------------------
		KANJI code Filter
-----------------------------------------------------------------------------*/

#ifndef		WTKF_H
#define		WTKF_H


/* Includes */
#include	<stdlib.h>
#include	<string.h>


/* Defines */
#define	WT_JP_CODE_ESC			(0x1b)
#define	WT_JP_CODE_STRMAX		(1024)
#define	WT_JP_CODE_STRMIN		(256)


/* Functions */
extern	int		checkkanjicode(unsigned char *);
extern	unsigned char	*change2euc(unsigned char *);
extern	unsigned char	*jis2euc(unsigned char *);
extern	unsigned char	*sjis2euc(unsigned char *);


#endif		/* WTKF_H */


/*=============================================================================
			end
=============================================================================*/
