#==============================================================================
#		Makefile for e2ps
#			by Nobuyuki SHIRAKI
#			Last change : Mon 20 May 2002  11:41:41
#==============================================================================


# Directory to install e2ps
E2PS_INST_DIR = /usr/local/bin

# Tab width
TABSTOP = 8

# Margine of top, bottom, left and right
TOP    = 40.0
BOTTOM = 40.0
LEFT   = 40.0
RIGHT  = 40.0

# Max lines per one page
MAXLINE = 66

# Default font size
FONTSIZE = 10.0

# Space ratio between lines
NLRATE = 1.1

# Paper settings
DEFPAPER = \
		-DTABSTOP=$(TABSTOP) -DMAXLINE=$(MAXLINE) \
		-DFONTSIZE=$(FONTSIZE) \
		-DTOP=$(TOP) -DBOTTOM=$(BOTTOM) \
		-DLEFT=$(LEFT) -DRIGHT=$(RIGHT) \
		-DNLRATE=$(NLRATE)

# If you use a printer without EUC fonts
DEFNOEUC = -DJIS_PS

# for Tektronix printers
# DEFTEKTRO = -DTEKTRO

# For Y2K
# Y2K = 0	-> System time + 1900 = year
# Y2K = 1	-> System time        = year
# Y2K = 2	-> System time + 2000 = year
DEFY2K = -DY2K=0


# Defines
DEFINES = $(DEFPAPER) $(DEFNOEUC) $(DEFTEKTRO) $(DEFY2K)


# C and C args
CC = gcc
CC_ARGS = -Wall
# CC_ARGS = -Wall -O2
CC_LIB = 

# strip command
STRIP = strip

CFLAGS = $(CC_ARGS) $(DEFINES) $(CC_LIB)

SRC = e2ps.c ps-font.c wtkf.c
OBJ = e2ps.o ps-font.o wtkf.o

all:		e2ps

clean:
	rm -f *~ e2ps *.o

readme:
	nkf -j     README.euc > README.jis
	nkf -s -Lw README.euc > README.sjis

install:	e2ps
	cp e2ps $(E2PS_INST_DIR)
	@if [ -f $(E2PS_INST_DIR)/e2lpr ]; then rm $(E2PS_INST_DIR)/e2lpr; fi
	ln -s $(E2PS_INST_DIR)/e2ps $(E2PS_INST_DIR)/e2lpr

e2ps:	$(OBJ)
	$(CC) -o $@ $(OBJ) $(CFLAGS)
	$(STRIP) $@
