/*=============================================================================
		e2ps.c
			by Nobuyuki SHIRAKI
			Last change : Wed 21 August 2002  17:51:03
=============================================================================*/

/*-----------------------------------------------------------------------------
		e2ps  changes EUC code text to JIS code PostScript
-----------------------------------------------------------------------------*/


#include	"e2ps.h"
#include	"wtkf.h"


/* Global variables */
uchar	*gsCommand;		/* own file name */
FILE	*gpfOut;
float	gafFontsize[2][256];
char	gsEscapedStr[3];
char	*gsAsciiFont = NULL, *gsKanjiFont = NULL;
char	*gsBoldAsciiFont = NULL, *gsBoldItalicAsciiFont = NULL;
char	*gsBoldKanjiFont = NULL, *gsBoldItalicKanjiFont = NULL;
int	giWidth, giHeight;
int	giPage, giPsPage;
uchar	guStyle;
uchar	guBox;
uchar	guHeader;
int	giMaxLine, giLine;
float	gfTopMargin, gfBottomMargin, gfLeftMargin, gfRightMargin;
float	gfX, gfY;
float	gfFontSize, gfNlRate;
float	gfAsciiWidth, gfAsciiHeight;
float	gfKanjiWidth, gfKanjiHeight;
char	gsNowTime[STRMAX], gsHeadStr[STRMAX], gsPageStr[STRMAX];
uchar	guJapanese = 1;

#ifdef	JIS_PS
uchar	guKanjiCode = 0;
#else
uchar	guKanjiCode = 1;
#endif

char *gsMonth[12] = {
  "Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" 
};


/* Main */
int main(int argc, char **argv) {
  FILE		*read;
  struct tm	*systime;
  time_t	nowtime;
  char		*sFileNames[256];
  char		*sPrinter = NULL;
  char		sPaper[10];
  char		sStr[STRMAX*10];
  signed char	ch;
  uchar		*sFileData = NULL, *sTmp = NULL;
  char		*sLang;
  int		iFiles = 0, iFileNum;
  int		iCharCount;
  uchar		uLpr = 0;
  uchar		uMode;		/* Printing ASCII or Kanji */
  int		i, j, index;
  long		lFileSize;
  float		fMaxWidth;
  float		ftmp;
#ifdef	TEKTRO
  uchar		uCopy = 1;	/* Number of paper */
  uchar		uDup = 0;	/* Both side */
#endif

  /* Signals */
  signal(SIGQUIT, quit);
  signal(SIGHUP,  quit);
  signal(SIGTERM, quit);
  signal(SIGINT,  quit);

  /* own file name */
  if ( argc > 0 ) {
    while ( strstr(argv[0], "/") ) {
      argv[0] = strstr(argv[0], "/") + 1;
    }
    gsCommand = setstring(argv[0]);
  }
  else {
    gsCommand = setstring(NAME);
  }

  /* Date and Time */
  nowtime = time(NULL);
  systime = localtime(&nowtime);
  switch ( Y2K ) {
  case 0:
  default:
    systime->tm_year += 1900;
    break;
  case 1:
    break;
  case 2:
    systime->tm_year += 2000;
    break;
  }
  sprintf(gsNowTime, "%s %.2d %.4d %.2d:%.2d:%.2d",
	  gsMonth[systime->tm_mon], systime->tm_mday, systime->tm_year,
	  systime->tm_hour, systime->tm_min, systime->tm_sec);

  /* Header string */
  gsHeadStr[0] = '\0';

  /* Page string */
  strcpy(gsPageStr, "Page");

  /* Other default */
  guStyle = 0x11;					/* Portrait */
  gsAsciiFont = setstring(ASCII);
  gsBoldAsciiFont = setstring(ASCII_BOLD);
  gsBoldItalicAsciiFont = setstring(ASCII_BOLDITALIC);
  if ( guKanjiCode ) {
    gsKanjiFont = setstring(EUC_KANJI);
    gsBoldKanjiFont = setstring(EUC_KANJI_BOLD);
    gsBoldItalicKanjiFont = setstring(EUC_KANJI_BOLDITALIC);
  }
  else {
    gsKanjiFont = setstring(KANJI);
    gsBoldKanjiFont = setstring(KANJI_BOLD);
    gsBoldItalicKanjiFont = setstring(KANJI_BOLDITALIC);
  }
  giMaxLine		= MAXLINE;
  strcpy(sPaper, "A4");
  giWidth		= A4WIDTH;
  giHeight		= A4HEIGHT;
  gfFontSize		= FONTSIZE;
  gfTopMargin		= TOP;
  gfBottomMargin	= BOTTOM;
  gfLeftMargin		= LEFT;
  gfRightMargin		= RIGHT;
  gfNlRate		= NLRATE;
  gfAsciiWidth		= ASCIIWIDTH;
  gfAsciiHeight		= ASCIIHEIGHT;
  gfKanjiWidth		= KANJIWIDTH;
  gfKanjiHeight		= KANJIHEIGHT;
  guHeader		= 1;
  guBox			= 0;

  /* Languages */
  sLang = (char *)getenv("LANG");
  if ( sLang != NULL &&
      ( !strcmp(sLang, "ja")
       || !strcmp(sLang, "japanese")
       || !strcmp(sLang, "ja_JP.ujis")
       || !strcmp(sLang, "ja_JP.eucJP")
       || !strcmp(sLang, "ja_JP.EUC")
       ) ) {
    guJapanese = 1;
  }
  else {
    guJapanese = 0;
  }

  /* Printer */
  if ( (char *)getenv("PRINTER") != NULL ) {
    sPrinter = setstring((char *)getenv("PRINTER"));
  }
  else {
    sPrinter = NULL;
  }
  /* Use printer */
  if ( strstr(argv[0], LPRNAME) ) {
    /* Printer set */
    uLpr = 1;
  }
  else {
    /* Standard Out */
    gpfOut = stdout;
  }

  /* Args */
  for ( i = 1 ; i < argc ; i++ ) {
    if ( strcmp(argv[i], "-l") == 0 ) {
      /* Landscape */
      guStyle = 0x00;
      continue;
    }
    else if ( strcmp(argv[i], "-l2") == 0 ) {
      /* Landscape Double */
      guStyle = 0x01;
      continue;
    }
    else if ( strcmp(argv[i], "-l4") == 0 ) {
      /* Landscape Quartet */
      guStyle = 0x02;
      continue;
    }
    else if ( strcmp(argv[i], "-lv4") == 0 ) {
      /* Landscape Quartet Vertical */
      guStyle = 0x03;
      continue;
    }
    else if ( strcmp(argv[i], "-p") == 0 ) {
      /* Portrait */
      guStyle = 0x10;
      continue;
    }
    else if ( strcmp(argv[i], "-p2") == 0 ) {
      /* Portrait Double */
      guStyle = 0x11;
      continue;
    }
    else if ( strcmp(argv[i], "-p4") == 0 ) {
      /* Portrait Quartet */
      guStyle = 0x12;
      continue;
    }
    else if ( strcmp(argv[i], "-pv4") == 0 ) {
      /* Portrait Quartet Vertical */
      guStyle = 0x13;
      continue;
    }
    else if ( strcmp(argv[i], "-ohp") == 0 ) {
      /* OHP printing */
      gfFontSize = OHPFONTSIZE;
      guStyle = 0x10;
      guHeader = 0;
      continue;
    }
    else if ( strcmp(argv[i], "-box") == 0 ) {
      /* Box printing */
      guBox = 1;
      continue;
    }
    else if ( strcmp(argv[i], "-af") == 0 ) {
      /* ASCII Font */
      i++;
      if ( i < argc ) {
	if ( gsAsciiFont != NULL ) free(gsAsciiFont);
	gsAsciiFont = setstring(argv[i]);
      }
      continue;
    }
    else if ( strcmp(argv[i], "-abf") == 0 ) {
      /* ASCII Bold Font */
      i++;
      if ( i < argc ) {
	if ( gsBoldAsciiFont != NULL ) free(gsBoldAsciiFont);
	gsBoldAsciiFont = setstring(argv[i]);
      }
      continue;
    }
    else if ( strcmp(argv[i], "-abif") == 0 ) {
      /* ASCII Bold Font Italic */
      i++;
      if ( i < argc ) {
	if ( gsBoldItalicAsciiFont != NULL ) free(gsBoldItalicAsciiFont);
	gsBoldItalicAsciiFont = setstring(argv[i]);
      }
      continue;
    }
    else if ( strcmp(argv[i], "-size") == 0 ) {
      /* Font Size */
      i++;
      if ( i < argc ) {
	gfFontSize = atof(argv[i]);
	if ( gfFontSize == 0.0 ) gfFontSize = FONTSIZE;
      }
      continue;
    }
    else if ( strcmp(argv[i], "-afw") == 0 ) {
      /* ASCII Font Width */
      i++;
      if ( i < argc ) {
	gfAsciiWidth = atof(argv[i]);
	if ( gfAsciiWidth == 0.0 ) gfAsciiWidth = ASCIIWIDTH;
      }
      continue;
    }
    else if ( strcmp(argv[i], "-afh") == 0 ) {
      /* ASCII Font Height */
      i++;
      if ( i < argc ) {
	gfAsciiHeight = atof(argv[i]);
	if ( gfAsciiHeight == 0.0 || gfAsciiHeight > ASCIIHEIGHT )
	  gfAsciiHeight = ASCIIHEIGHT;
      }
      continue;
    }
    else if ( strcmp(argv[i], "-kfw") == 0 ) {
      /* KANJI Font Width */
      i++;
      if ( i < argc ) {
	gfKanjiWidth = atof(argv[i]);
	if ( gfKanjiWidth == 0.0 ) gfKanjiWidth = KANJIWIDTH;
      }
      continue;
    }
    else if ( strcmp(argv[i], "-kfh") == 0 ) {
      /* KANJI Font Height */
      i++;
      if ( i < argc ) {
	gfKanjiHeight = atof(argv[i]);
	if ( gfKanjiHeight == 0.0 || gfKanjiHeight > KANJIHEIGHT )
	  gfKanjiHeight = KANJIHEIGHT;
      }
      continue;
    }
    else if ( strcmp(argv[i], "-ls") == 0 ) {
      /* Space ratio */
      i++;
      if ( i < argc ) {
	gfNlRate = atof(argv[i]);
	if ( gfNlRate == 0.0 ) gfNlRate = NLRATE;
      }
      continue;
    }
    else if ( strcmp(argv[i], "-line") == 0 ) {
      /* Max Lines per page */
      i++;
      if ( i < argc ) {
	giMaxLine = atoi(argv[i]);
	if ( giMaxLine < 1 ) giMaxLine = MAXLINE;
      }
      continue;
    }
    else if ( strcmp(argv[i], "-a4") == 0 ) {
      /* A4 */
      strcpy(sPaper, "A4");
      giWidth = A4WIDTH;
      giHeight = A4HEIGHT;
    }
    else if ( strcmp(argv[i], "-b4") == 0 ) {
      /* B4 */
      strcpy(sPaper, "B4");
      giWidth = B4WIDTH;
      giHeight = B4HEIGHT;
    }
    else if ( strcmp(argv[i], "-le") == 0 ) {
      /* Letter */
      strcpy(sPaper, "LETTER");
      giWidth = LEWIDTH;
      giHeight = LEHEIGHT;
    }
    else if ( strcmp(argv[i], "-tm") == 0 ) {
      /* Top margin */
      i++;
      if ( i < argc ) {
	gfTopMargin = atof(argv[i]);
	if ( gfTopMargin == 0.0 ) gfTopMargin = TOP;
      }
      continue;
    }
    else if ( strcmp(argv[i], "-bm") == 0 ) {
      /* Bottom margin */
      i++;
      if ( i < argc ) {
	gfBottomMargin = atof(argv[i]);
	if ( gfBottomMargin == 0.0 ) gfBottomMargin = BOTTOM;
      }
      continue;
    }
    else if ( strcmp(argv[i], "-lm") == 0 ) {
      /* Left margin */
      i++;
      if ( i < argc ) {
	gfLeftMargin = atof(argv[i]);
	if ( gfLeftMargin == 0.0 ) gfLeftMargin = LEFT;
      }
      continue;
    }
    else if ( strcmp(argv[i], "-rm") == 0 ) {
      /* Right margin */
      i++;
      if ( i < argc ) {
	gfRightMargin = atof(argv[i]);
	if ( gfRightMargin == 0.0 ) gfRightMargin = RIGHT;
      }
      continue;
    }
    else if ( strcmp(argv[i], "-P") == 0 ) {
      /* Printer */
      i++;
      if ( i < argc ) {
	if ( sPrinter != NULL ) free(sPrinter);
	sPrinter = setstring(argv[i]);
	uLpr = 1;
      }
      continue;
    }
    else if ( argv[i][0] == '-' && argv[i][1] == 'P' && argv[i][2] != '\0' ) {
      /* Printer */
      if ( sPrinter != NULL ) free(sPrinter);
      sPrinter = setstring(argv[i]+2);
      uLpr = 1;
      continue;
    }
    else if ( strcmp(argv[i], "-date") == 0 ) {
      /* Date string */
      i++;
      if ( i < argc ) {
	strcpy(gsNowTime, argv[i]);
      }
      continue;
    }
    else if ( strcmp(argv[i], "-head") == 0 ) {
      /* Header string */
      i++;
      if ( i < argc ) {
	strcpy(gsHeadStr, argv[i]);
      }
      continue;
    }
    else if ( strcmp(argv[i], "-page") == 0 ) {
      /* Page string */
      i++;
      if ( i < argc ) {
	strcpy(gsPageStr, argv[i]);
      }
      continue;
    }
    else if ( strcmp(argv[i], "-nh") == 0 ) {
      /* No header */
      guHeader = 0;
      continue;
    }
    else if ( strcmp(argv[i], "-e") == 0 ) {
      /* EUC printing */
      guKanjiCode = 1;
      if ( gsKanjiFont           != NULL ) free(gsKanjiFont);
      if ( gsBoldKanjiFont       != NULL ) free(gsBoldKanjiFont);
      if ( gsBoldItalicKanjiFont != NULL ) free(gsBoldItalicKanjiFont);
      gsKanjiFont = setstring(EUC_KANJI);
      gsBoldKanjiFont = setstring(EUC_KANJI_BOLD);
      gsBoldItalicKanjiFont = setstring(EUC_KANJI_BOLDITALIC);
      continue;
    }
    else if ( strcmp(argv[i], "-j") == 0 ) {
      /* JIS printing */
      guKanjiCode = 0;
      if ( gsKanjiFont           != NULL ) free(gsKanjiFont);
      if ( gsBoldKanjiFont       != NULL ) free(gsBoldKanjiFont);
      if ( gsBoldItalicKanjiFont != NULL ) free(gsBoldItalicKanjiFont);
      gsKanjiFont = setstring(KANJI);
      gsBoldKanjiFont = setstring(KANJI_BOLD);
      gsBoldItalicKanjiFont = setstring(KANJI_BOLDITALIC);
      continue;
    }
#ifdef	TEKTRO
    else if ( strcmp(argv[i], "-copy") == 0 ) {
      /* Number of paper */
      i++;
      if ( i < argc ) {
	uCopy = atoi(argv[i]);
      }
      else {
	uCopy = 1;
      }
      if ( uCopy < 1 ) uCopy = 1;
      continue;
    }
    else if ( strcmp(argv[i], "-dup") == 0 ) {
      /* Both side */
      uDup = 1;
      continue;
    }
#endif
    else if ( strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0 ) {
      /* Help */
      help(NULL);
    }
    else if ( strcmp(argv[i], "-v") == 0 || strcmp(argv[i], "--version") == 0 ) {
      /* Version */
      while ( strstr(argv[0], "/") ) {
	argv[0] = strstr(argv[0], "/") + 1;
      }
      if ( guJapanese ) {
	printf("%s の現在のバージョンは Version %.2f (%s) です。\n", argv[0], VERSION/100.0, DATE);
      }
      else {
	printf("%s's version is %.2f (%s).\n", argv[0], VERSION/100.0, DATE);
      }
      exit(0);
    }
    else if ( strcmp(argv[i], "-fl") == 0 ) {
      /* Print Font List */
      if ( guJapanese ) {
	printf("\nフォントリスト\n");
	printf("%s", ps_font(NULL, NULL));
	/*  printf("Ryumin-Light-H\n"); */
	/*  printf("Ryumin-Light-H-Italic\n"); */
	/*  printf("Ryumin-Light-EUC-H\n"); */
	/*  printf("Ryumin-Light-EUC-H-Italic\n"); */
	/*  printf("GothicBBB-Medium-H\n"); */
	/*  printf("GothicBBB-Medium-H-Italic\n"); */
	/*  printf("GothicBBB-Medium-EUC-H\n"); */
	/*  printf("GothicBBB-Medium-EUC-H-Italic\n\n"); */
      }
      else {
	printf("\nFont Lists\n");
	printf("%s", ps_font(NULL, NULL));
	/*  printf("Ryumin-Light-H\n"); */
	/*  printf("Ryumin-Light-H-Italic\n"); */
	/*  printf("Ryumin-Light-EUC-H\n"); */
	/*  printf("Ryumin-Light-EUC-H-Italic\n"); */
	/*  printf("GothicBBB-Medium-H\n"); */
	/*  printf("GothicBBB-Medium-H-Italic\n"); */
	/*  printf("GothicBBB-Medium-EUC-H\n"); */
	/*  printf("GothicBBB-Medium-EUC-H-Italic\n\n"); */
      }
      exit(0);
    }
    else if ( strcmp(argv[i], "-lib") == 0 ) {
      /* Environment */
      /* Version */
      while ( strstr(argv[0], "/") ) {
	argv[0] = strstr(argv[0], "/") + 1;
      }
      if ( guJapanese ) {
	printf("コマンド名               : %s\n", argv[0]);
	printf("製作者                   : %s\n", CREATOR);
	printf("バージョン               : Version %.2f\n", VERSION/100.0);
	printf("製作日                   : %s\n", DATE);
	printf("フォントサイズ           : %.2f\n", FONTSIZE);
	printf("タブ幅                   : %d\n", TABSTOP);
	printf("改行率                   : %.2f\n", NLRATE);
	printf("最大行数                 : %d\n", MAXLINE);
	printf("用紙上余白               : %.2f\n", TOP);
	printf("用紙下余白               : %.2f\n", BOTTOM);
	printf("用紙左余白               : %.2f\n", LEFT);
	printf("用紙右余白               : %.2f\n", RIGHT);
	printf("アスキー印刷横倍率       : %.2f\n", gfAsciiWidth);
	printf("アスキー印刷縦倍率       : %.2f\n", gfAsciiHeight);
	printf("アスキー太字フォント     : %s\n", gsBoldAsciiFont);
	printf("アスキー太字斜体フォント : %s\n", gsBoldItalicAsciiFont);
	printf("漢字印刷横倍率           : %.2f\n", gfKanjiWidth);
	printf("漢字印刷縦倍率           : %.2f\n", gfKanjiHeight);
	printf("漢字フォント             : %s\n", gsKanjiFont);
	printf("漢字太字フォント         : %s\n", gsBoldKanjiFont);
	printf("漢字太字斜体フォント     : %s\n", gsBoldItalicKanjiFont);
      }
      else {
	printf("Command Name             : %s\n", argv[0]);
	printf("Creator                  : %s\n", CREATOR);
	printf("Version                  : Version %.2f\n", VERSION/100.0);
	printf("Created Date             : %s\n", DATE);
	printf("Font Size                : %.2f\n", FONTSIZE);
	printf("Tab Width                : %d\n", TABSTOP);
	printf("Space Ratio              : %.2f\n", NLRATE);
	printf("Max Lines                : %d\n", MAXLINE);
	printf("Top    Margin            : %.2f\n", TOP);
	printf("Bottom Margin            : %.2f\n", BOTTOM);
	printf("Left   Margin            : %.2f\n", LEFT);
	printf("Right  Margin            : %.2f\n", RIGHT);
	printf("ASCII Font Print Width   : %.2f\n", gfAsciiWidth);
	printf("ASCII Font Print Height  : %.2f\n", gfAsciiHeight);
	printf("ASCII Font               : %s\n", gsAsciiFont);
	printf("ASCII Bold Font          : %s\n", gsBoldAsciiFont);
	printf("ASCII Bold Italic Font   : %s\n", gsBoldItalicAsciiFont);
	printf("KANJI Font Print Width   : %.2f\n", gfKanjiWidth);
	printf("KANJI Font Print Height  : %.2f\n", gfKanjiHeight);
	printf("KANJI Font               : %s\n", gsKanjiFont);
	printf("KANJI Bold Font          : %s\n", gsBoldKanjiFont);
	printf("KANJI Bold Italic Font   : %s\n", gsBoldItalicKanjiFont);
      }
      exit(0);
    }
    else if ( ( read = fopen(argv[i], "r") ) != NULL ) {
      /* File existence */
      fclose(read);
      if ( iFiles < 256 ) {
	sFileNames[iFiles] = argv[i];
	iFiles++;
      }
      else {
	if ( guJapanese ) {
	  strcpy(sStr, "指定したファイル数が256を越えました。");
	}
	else {
	  strcpy(sStr, "Files for printing are over 256.");
	}
	exit(0);
      }
    }
    else {
      /* Mistake of arg */
      if ( guJapanese ) {
	sprintf(sStr, "引数に間違いがあります。==> %s", argv[i]);
      }
      else {
	sprintf(sStr, "Wrong argment==> %s", argv[i]);
      }
      help(sStr);
    }
  }

  /* Set Max Lines */
  if ( guStyle >= 0x10 &&
       !strcmp(gsAsciiFont, "Courier") &&
       giMaxLine == MAXLINE &&
       giWidth   == A4WIDTH &&
       giHeight  == A4HEIGHT &&
       gfFontSize  == FONTSIZE &&
       (int)(gfNlRate*10) == (int)(NLRATE*10) ) {
    giMaxLine = MAXLINE;
  }

  /* Pipe */
  if ( uLpr ) {
    if ( sPrinter != NULL ) {
      sprintf(sStr, "lpr -P %s", sPrinter);
    }
    else {
      strcpy(sStr, "lpr");
    }
    if ( ( gpfOut = popen(sStr, "w") ) == NULL ) {
      if ( guJapanese ) {
	fprintf(stderr, "パイプがつながりません。\n");
      }
      else {
	fprintf(stderr, "Cannot make pipe.\n");
      }
      gpfOut = stdout;
    }
  }

  /* Standard In */
  if ( iFiles == 0 ) {
    sFileNames[iFiles++] = "stdin";
  }

  /* Start Input and Output */
  /* Header */
  fprintf(gpfOut, "%%!PS-Adobe-2.0 EPSF-1.2\n");
  fprintf(gpfOut, "%%%%Title:");
  for ( iFileNum = 0 ; iFileNum < iFiles ; iFileNum++ ) {
    fprintf(gpfOut, " %s", sFileNames[iFileNum]);
  }
  fprintf(gpfOut, "\n");
  fprintf(gpfOut, "%%%%Creator: %s\n", CREATOR);
  fprintf(gpfOut, "%%%%CreationDate: %s\n", gsNowTime);
  fprintf(gpfOut, "%%%%Pages: (atend)\n");
  fprintf(gpfOut, "%%%%PageOrder: Ascend\n");
  fprintf(gpfOut, "%%%%DocumentPaperSizes: %s\n", sPaper);
  fprintf(gpfOut, "%%%%Orientation: Portrait\n");
#ifdef	TEKTRO
  /* Number of paper */
  fprintf(gpfOut, "%%%%Requirements: numcopies(%d) collate\n", uCopy);
#endif
  fprintf(gpfOut, "%%%%EndComments\n\n");
#ifdef	TEKTRO
  /* Number of Paper */
  fprintf(gpfOut, "/#copies %d def\n\n", uCopy);
  /* Both side */
  if ( uDup ) {
    fprintf(gpfOut, "[{\n");
    fprintf(gpfOut, "%%%%BeginFeature: *Duplex DuplexTumble\n\n");
    fprintf(gpfOut, "        <</Duplex true /Tumble false>> setpagedevice\n");
    fprintf(gpfOut, "%%%%EndFeature\n");
    fprintf(gpfOut, "} stopped cleartomark\n\n");
  }
#endif

  /* Set Fonts */
  fprintf(gpfOut, "/A-R { /%s findfont %.2f scalefont [1 0 0 1 0 0.12] makefont setfont } bind def\n", gsAsciiFont, gfFontSize);
  fprintf(gpfOut, "/A-B { /%s findfont %.2f scalefont [1 0 0 1 0 0.12] makefont setfont } bind def\n", gsBoldAsciiFont, gfFontSize);
  fprintf(gpfOut, "/A-BI { /%s findfont %.2f scalefont [1 0 0 1 0 0.12] makefont setfont } bind def\n", gsBoldItalicAsciiFont, gfFontSize);
  fprintf(gpfOut, "/K-R { /%s findfont %.2f scalefont setfont } bind def\n", gsKanjiFont, gfFontSize);
  fprintf(gpfOut, "/K-B { /%s findfont %.2f scalefont setfont } bind def\n", gsBoldKanjiFont, gfFontSize);
  fprintf(gpfOut, "/K-BI { /%s findfont %.2f scalefont [1 0 0.3 1 0 0] makefont setfont } bind def\n", gsBoldKanjiFont, gfFontSize);
  fprintf(gpfOut, "\n");
  if ( guKanjiCode ) {
    fprintf(gpfOut, "%% Copy Font\n");
    fprintf(gpfOut, "/copyfont {\n");
    fprintf(gpfOut, "    dup maxlength 1 add dict begin\n");
    fprintf(gpfOut, "    {\n");
    fprintf(gpfOut, "        1 index /FID ne 2 index /UniqueID ne and\n");
    fprintf(gpfOut, "        {def} {pop pop} ifelse\n");
    fprintf(gpfOut, "    } forall\n");
    fprintf(gpfOut, "    currentdict\n");
    fprintf(gpfOut, "    end\n");
    fprintf(gpfOut, "} bind def\n\n");
    fprintf(gpfOut, "%% Merge Font  From tgif\n");
    fprintf(gpfOut, "/mergefont {\n");
    fprintf(gpfOut, "    12 dict begin\n");
    fprintf(gpfOut, "        dup type /nametype eq { findfont } if\n");
    fprintf(gpfOut, "        dup /WMode known {\n");
    fprintf(gpfOut, "            dup /WMode get /WMode exch def\n");
    fprintf(gpfOut, "            WMode 1 eq {\n");
    fprintf(gpfOut, "                [0.0 1.0 -1.0 0.0 0.0 0.3] makefont\n");
    fprintf(gpfOut, "            } if\n");
    fprintf(gpfOut, "        } if\n");
    fprintf(gpfOut, "        copyfont dup begin\n");
    fprintf(gpfOut, "            /Encoding Encoding\n");
    fprintf(gpfOut, "            FMapType dup 2 eq {\n");
    fprintf(gpfOut, "                pop 128 128\n");
    fprintf(gpfOut, "            } { 5 eq {\n");
    fprintf(gpfOut, "                256 256\n");
    fprintf(gpfOut, "            } {\n");
    fprintf(gpfOut, "                /compositefont errordict /invalidfont get exec\n");
    fprintf(gpfOut, "            } ifelse\n");
    fprintf(gpfOut, "            } ifelse\n");
    fprintf(gpfOut, "            getinterval def\n");
    fprintf(gpfOut, "        end\n");
    fprintf(gpfOut, "        /kanjifont exch definefont\n");
    fprintf(gpfOut, "        exch\n");
    fprintf(gpfOut, "\n");
    fprintf(gpfOut, "        dup type /nametype eq { findfont } if\n");
    fprintf(gpfOut, "        exch\n");
    fprintf(gpfOut, "\n");
    fprintf(gpfOut, "        /FDepVector [ 4 2 roll ] def\n");
    fprintf(gpfOut, "        /FontType 0 def\n");
    fprintf(gpfOut, "        /FMapType 4 def\n");
    fprintf(gpfOut, "        /FontMatrix matrix def\n");
    fprintf(gpfOut, "        /Encoding [ 0 1 ] def\n");
    fprintf(gpfOut, "        /FontBBox {0 0 0 0} def\n");
    fprintf(gpfOut, "        dup /FontName exch def\n");
    fprintf(gpfOut, "        currentdict\n");
    fprintf(gpfOut, "    end\n");
    fprintf(gpfOut, "    definefont pop\n");
    fprintf(gpfOut, "} def\n");
    fprintf(gpfOut, "\n");
    fprintf(gpfOut, "/HEAD /Helvetica /Ryumin-Light-EUC-H mergefont\n");
    fprintf(gpfOut, "/HEAD-BOLD /Helvetica-Bold /GothicBBB-Medium-EUC-H mergefont\n");
  }
  else {
    fprintf(gpfOut, "/HEAD            { /Helvetica } bind def\n");
    fprintf(gpfOut, "/HEAD-BOLD       { /Helvetica-Bold } bind def\n");
    fprintf(gpfOut, "/HEAD-KANJI      { /Ryumin-Light-H } bind def\n");
    fprintf(gpfOut, "/HEAD-KANJI-BOLD { /GothicBBB-Medium-H } bind def\n");
  }
  fprintf(gpfOut, "\n\n");

  /* Page */
  giPsPage = 0;
  
  if ( guStyle < 0x10 ) {
    /* Exchange */
    ftmp = giWidth;
    giWidth = giHeight;
    giHeight = ftmp;
  }

  /* Max Width */
  fMaxWidth = giWidth - gfRightMargin;

  /* File Loop */
  for ( iFileNum = 0 ; iFileNum < iFiles ; iFileNum++ ) {
    sFileData = NULL;
    /* Read from ? */
    if ( strcmp(sFileNames[iFileNum], "stdin") ) {
      /* File */
      read = fopen(sFileNames[iFileNum], "r");
      fseek(read, 0, SEEK_END);
      lFileSize = ftell(read);
      rewind(read);
    }
    else {
      /* Standard Input */
      read = stdin;
      lFileSize = 0;
    }
    /* Read Data */
    if ( lFileSize > 0 ) {
      /* From File */
      sFileData = setstringsize(lFileSize);
      fread(sFileData, sizeof(char), lFileSize, read);
    }
    else {
      /* From Standard Input */
      while ( fgets(sStr, STRMAX, read) ) {
	sFileData = addstring(sFileData, sStr);
      }
    }
    fclose(read);

    /* If file has no data, go to next file */
    if ( sFileData == NULL ) continue;

    /* Change to EUC */
    sTmp = change2euc(sFileData);
    free(sFileData);
    sFileData = sTmp;
    
    /* Setting before printing */
    giPage = 0;
    giLine = 1;
    newpage(sFileNames[iFileNum]);
    uMode = 0;
    iCharCount = 0;
    ps_font(gsAsciiFont, gafFontsize[0]);
    ps_font(gsBoldItalicAsciiFont, gafFontsize[1]);
    sStr[0] = '\0';
    for ( index = 0 ; ( ch = *(sFileData+index) ) != '\0' ; index++ ) {
      if ( ch == NEWPAGE ) {
	/* New Page */
	newpage(sFileNames[iFileNum]);
	giLine = 1;
	continue;
      }
      if ( ch == NEWLINE ) {
	/* New Line */
	newline(uMode, sStr, sFileNames[iFileNum]);
	sStr[0] = '\0';
	iCharCount = 0;
	continue;
      }
      if ( ch == TAB ) {
	/* TAB */
	if ( uMode != 0 ) {
	  print(uMode, sStr);
	  sStr[0] = '\0';
	}
	for ( j = 0 ; j < TABSTOP - ( iCharCount % TABSTOP ) ; j++ )
	  strcat(sStr, " ");
	uMode = 0;
	/* String width */
	gfX += gafFontsize[0][' '] * gfAsciiWidth/ASCIIWIDTH *
	  ( TABSTOP - ( iCharCount % TABSTOP ) ) * gfFontSize;
	iCharCount += TABSTOP - ( iCharCount % TABSTOP );
	/* New Line? */
	if ( gfX >= fMaxWidth && *(sFileData+index+1) != '\n' ) {
	  newline(uMode, sStr, sFileNames[iFileNum]);
	  sStr[0] = '\0';
	  iCharCount = 0;
	}
	continue;
      }
      if ( ch == '_' && *(sFileData+index+1) == BACKSPACE ) {
	/* Back Space */
	if ( ( *(sFileData+index+2) & 128 ) == 0 ) {
	  if ( uMode != 2 ) {
	    print(uMode, sStr);
	    sStr[0] = '\0';
	  }
	  strcat(sStr, escape(*(sFileData+index+2)));
	  index += 2;
	  iCharCount++;
	  uMode = 2;
	  /* String width */
	  /*  ps_font(gsBoldItalicAsciiFont, gafFontsize); */
	  gfX += gafFontsize[1][(int)*(sFileData+index+2)] *
	    gfAsciiWidth/ASCIIWIDTH * gfFontSize;
	  /*  ps_font(gsAsciiFont, gafFontsize); */
	  /* New Line? */
	  if ( gfX >= fMaxWidth && *(sFileData+index+1) != '\n' ) {
	    newline(uMode, sStr, sFileNames[iFileNum]);
	    sStr[0] = '\0';
	    iCharCount = 0;
	  }
	}
	else {
	  if ( uMode != 3 ) {
	    print(uMode, sStr);
	    sStr[0] = '\0';
	  }
	  strcat(sStr, escape(*(sFileData+index+2)));
	  strcat(sStr, escape(*(sFileData+index+3)));
	  index += 3;
	  iCharCount++;
	  iCharCount++;
	  uMode = 3;
	  /* String width */
	  /*  gfX += gfFontSize * 1.2; */
	  gfX += gfFontSize * gfKanjiWidth;
	  /* New Line? */
	  if ( gfX >= fMaxWidth && *(sFileData+index+1) != '\n' ) {
	    newline(uMode, sStr, sFileNames[iFileNum]);
	    sStr[0] = '\0';
	    iCharCount = 0;
	  }
	}
	continue;
      }
      if ( ( ch & 0x80 ) == 0 ) {
	if ( uMode != 0 ) {
	  print(uMode, sStr);
	  sStr[0] = '\0';
	}
	strcat(sStr, escape(ch));
	iCharCount++;
	uMode = 0;
	/* String width */
	gfX += gafFontsize[0][(int)ch] * gfAsciiWidth/ASCIIWIDTH * gfFontSize;
	/* New Line? */
	if ( gfX >= fMaxWidth && *(sFileData+index+1) != '\n' ) {
	  newline(uMode, sStr, sFileNames[iFileNum]);
	  sStr[0] = '\0';
	  iCharCount = 0;
	}
      }
      else {
	if ( uMode != 1 ) {
	  print(uMode, sStr);
	  sStr[0] = '\0';
	}
	strcat(sStr, escape(ch));
	ch = *(sFileData+(++index));
	strcat(sStr, escape(ch));
	iCharCount++;
	iCharCount++;
	uMode = 1;
	/* String width */
	/*  gfX += gfFontSize * 1.2; */
	gfX += gfFontSize * gfKanjiWidth;
	/* New Line? */
	if ( gfX >= fMaxWidth && *(sFileData+index+1) != '\n' ) {
	  newline(uMode, sStr, sFileNames[iFileNum]);
	  sStr[0] = '\0';
	  iCharCount = 0;
	}
      }
    }
    print(uMode, sStr);
    sStr[0] = '\0';
    iCharCount = 0;
    fprintf(gpfOut, "\n\ngrestore\n\nshowpage\n\n");
    free(sFileData);
  }
  /* Finish */
  fprintf(gpfOut, "%%%%Trailer\n");

  if ( uLpr ) {
    pclose(gpfOut);
  }
  else {
    fclose(gpfOut);
  }

  /* Number of pages */
  if ( guJapanese ) {
    fprintf(stderr, "%% %d ページ出力しました\n", giPsPage);
  }
  else {
    if ( giPsPage == 1 ) {
      fprintf(stderr, "%% %d pages were outputed.\n", giPsPage);
    }
    else {
      fprintf(stderr, "%% One page was outputed.\n");
    }
  }

  /* Free Area */
  if ( gsCommand             != NULL ) free(gsCommand);
  if ( gsAsciiFont           != NULL ) free(gsAsciiFont);
  if ( gsBoldAsciiFont       != NULL ) free(gsBoldAsciiFont);
  if ( gsBoldItalicAsciiFont != NULL ) free(gsBoldItalicAsciiFont);
  if ( gsKanjiFont           != NULL ) free(gsKanjiFont);
  if ( gsBoldKanjiFont       != NULL ) free(gsBoldKanjiFont);
  if ( gsBoldItalicKanjiFont != NULL ) free(gsBoldItalicKanjiFont);
  if ( sPrinter              != NULL ) free(sPrinter);

  return 0;
}


/* Print */
void print(uchar uMode, char *sStr) {
  int	i;

  if ( sStr != NULL && *sStr != '\0' ) {
    switch ( uMode ) {
    case 0:
      /* ASCII */
      print_font(gsAsciiFont);
      fprintf(gpfOut, "%.2f %.2f scale\n", gfAsciiWidth/ASCIIWIDTH, gfAsciiHeight);
      fprintf(gpfOut, "(%s) show\n", sStr);
      fprintf(gpfOut, "1.0 %.2f div 1.0 %.2f div scale\n", gfAsciiWidth/ASCIIWIDTH, gfAsciiHeight);
      break;
    case 1:
      /* KANJI */
      print_font(gsKanjiFont);
      if ( guKanjiCode ) {
	/*  fprintf(gpfOut, "%.2f 0 (%s) ashow\n", gfFontSize * 0.2, sStr); */
	if ( gfKanjiWidth >= 1.0 ) {
	  /* Kanji font (default width=1.2) */
	  fprintf(gpfOut, "%.2f 0 (%s) ashow\n", gfFontSize * (gfKanjiWidth-1.0), sStr);
	}
	else {
	  fprintf(gpfOut, "%.2f %.2f scale\n", gfKanjiWidth-0.2, gfKanjiHeight);
	  fprintf(gpfOut, "(%s) show\n", sStr);
	  fprintf(gpfOut, "1.0 %.2f div 1.0 %.2f div scale\n", gfKanjiWidth-0.2, gfKanjiHeight);
	}
      }
      else {
	/* For printers without EUC fonts */
	/* Change to JIS Code */
	/*  fprintf(gpfOut, "%.2f 0 (", gfFontSize * 0.2); */
	if ( gfKanjiWidth >= 1.0 ) {
	  fprintf(gpfOut, "%.2f 0 (", gfFontSize * (gfKanjiWidth-1.0));
	  for ( i = 0 ; *(sStr+i) != '\0' ; i++ ) {
	    fprintf(gpfOut, "\\%03o", *(sStr+i) & 0x7f);
	  }
	  fprintf(gpfOut, ") ashow\n");
	}
	else {
	  fprintf(gpfOut, "%.2f %.2f scale\n", gfKanjiWidth-0.2, gfKanjiHeight);
	  fprintf(gpfOut, "(");
	  for ( i = 0 ; *(sStr+i) != '\0' ; i++ ) {
	    fprintf(gpfOut, "\\%03o", *(sStr+i) & 0x7f);
	  }
	  fprintf(gpfOut, ") show\n");
	  fprintf(gpfOut, "1.0 %.2f div 1.0 %.2f div scale\n", gfKanjiWidth-0.2, gfKanjiHeight);
	}
      }
      break;
    case 2:
      /* Bold Italic ASCII */
      print_font(gsBoldItalicAsciiFont);
      fprintf(gpfOut, "%.2f %.2f scale\n", gfAsciiWidth/ASCIIWIDTH, gfAsciiHeight);
      fprintf(gpfOut, "(%s) show\n", sStr);
      fprintf(gpfOut, "1.0 %.2f div 1.0 %.2f div scale\n", gfAsciiWidth/ASCIIWIDTH, gfAsciiHeight);
      break;
    case 3:
      /* Bold Italic KANJI */
      print_font(gsBoldItalicKanjiFont);
      if ( guKanjiCode ) {
	/*  fprintf(gpfOut, "%.2f 0 (%s) ashow\n", gfFontSize * 0.2, sStr); */
	if ( gfKanjiWidth >= 1.0 ) {
	  fprintf(gpfOut, "%.2f 0 (%s) ashow\n", gfFontSize * (gfKanjiWidth-1.0), sStr);
	}
	else {
	  fprintf(gpfOut, "%.2f %.2f scale\n", gfKanjiWidth-0.2, gfKanjiHeight);
	  fprintf(gpfOut, "(%s) show\n", sStr);
	  fprintf(gpfOut, "1.0 %.2f div 1.0 %.2f div scale\n", gfKanjiWidth-0.2, gfKanjiHeight);
	}
      }
      else {
	/* For printers without EUC fonts */
	/* Change to JIS Code */
	/*  fprintf(gpfOut, "%.2f 0 (", gfFontSize * 0.2); */
	if ( gfKanjiWidth >= 1.0 ) {
	  fprintf(gpfOut, "%.2f 0 (", gfFontSize * (gfKanjiWidth-1.0));
	  for ( i = 0 ; *(sStr+i) != '\0' ; i++ ) {
	    fprintf(gpfOut, "\\%03o", *(sStr+i) & 0x7f);
	  }
	  fprintf(gpfOut, ") ashow\n");
	}
	else {
	  fprintf(gpfOut, "%.2f %.2f scale\n", gfKanjiWidth-0.2, gfKanjiHeight);
	  fprintf(gpfOut, "(");
	  for ( i = 0 ; *(sStr+i) != '\0' ; i++ ) {
	    fprintf(gpfOut, "\\%03o", *(sStr+i) & 0x7f);
	  }
	  fprintf(gpfOut, ") show\n");
	  fprintf(gpfOut, "1.0 %.2f div 1.0 %.2f div scale\n", gfKanjiWidth-0.2, gfKanjiHeight);
	}
      }
      break;
    }
  }

  return;
}


/* New Line */
void newline(uchar uMode, char *sStr, char *sFileName) {
  print(uMode, sStr);
  gfX = gfLeftMargin;
  gfY -= gfFontSize * gfNlRate;
  giLine++;
  /* New Page? */
  if ( gfY < gfLeftMargin || giLine > giMaxLine ) {
    /* New Page */
    newpage(sFileName);
    giLine = 1;
  }
  else {
    fprintf(gpfOut, "\n%.2f %.2f moveto\n", gfX, gfY);
  }

  return;
}


/* New Page */
void newpage(char *sFileName) {
  char	sStr[STRMAX];

  /* Page++ */
  giPage++;
  if ( giPage > 1 ) fprintf(gpfOut, "\n\ngrestore\n\n");
  /* PosctScript Page++? */
  if (
      ( guStyle == 0x00 ) ||
      ( guStyle == 0x01 && ( giPage % 2 ) == 1 ) ||
      ( guStyle == 0x02 && ( giPage % 4 ) == 1 ) ||
      ( guStyle == 0x03 && ( giPage % 4 ) == 1 ) ||
      ( guStyle == 0x10 ) ||
      ( guStyle == 0x11 && ( giPage % 2 ) == 1 ) ||
      ( guStyle == 0x12 && ( giPage % 4 ) == 1 ) ||
      ( guStyle == 0x13 && ( giPage % 4 ) == 1 )
      ) {
    giPsPage++;
    if ( giPage > 1 ) fprintf(gpfOut, "showpage\n\n");
    fprintf(gpfOut, "%%%%Page: %d %d\n\n", giPsPage, giPsPage);
    /* First Position */
    gfX = gfLeftMargin;
    gfY = giHeight - gfTopMargin;
    /* Message of Big File */
    if ( ( giPsPage % 100 ) == 0 ) {
      if ( guJapanese ) {
	fprintf(stderr, "%% %d ページ目出力中\n", giPsPage);
      }
      else {
	fprintf(stderr, "%% Page %d is being outputed...\n", giPsPage);
      }
    }
  }
  /* Keep PostScript State */
  fprintf(gpfOut, "gsave\n\n");
  switch ( guStyle ) {
  case 0x00:
    /* Landscape */
    fprintf(gpfOut, "90 rotate\n");
    fprintf(gpfOut, "0 -%d translate\n", giHeight);
    break;
  case 0x01:
    /* Landscape Double */
    fprintf(gpfOut, "%.2f %.2f scale\n", SQRT, SQRT);
    if ( ( giPage % 2 ) == 1 ) {
      /* Odd page */
      fprintf(gpfOut, "0 %d translate\n", giHeight);
    }
    break;
  case 0x02:
    /* Landscape Quartet */
    fprintf(gpfOut, "0.5 0.5 scale\n");
    fprintf(gpfOut, "90 rotate\n");
    switch ( giPage % 4 ) {
    case 1:
      fprintf(gpfOut, "0 -%d translate\n", giHeight);
      break;
    case 2:
      fprintf(gpfOut, "0 -%d translate\n", 2*giHeight);
      break;
    case 3:
      fprintf(gpfOut, "%d -%d translate\n", giWidth, giHeight);
      break;
    case 0:
      fprintf(gpfOut, "%d -%d translate\n", giWidth, 2*giHeight);
      break;
    }
    break;
  case 0x03:
    /* Landscape Quartet Vertical */
    fprintf(gpfOut, "0.5 0.5 scale\n");
    fprintf(gpfOut, "90 rotate\n");
    switch ( giPage % 4 ) {
    case 1:
      fprintf(gpfOut, "0 -%d translate\n", giHeight);
      break;
    case 2:
      fprintf(gpfOut, "%d -%d translate\n", giWidth, giHeight);
      break;
    case 3:
      fprintf(gpfOut, "0 -%d translate\n", 2*giHeight);
      break;
    case 0:
      fprintf(gpfOut, "%d -%d translate\n", giWidth, 2*giHeight);
      break;
    }
    break;
  case 0x10:
    /* Portrait */
    /* Do nothing */
    break;
  case 0x11:
    /* Portrait Double */
    fprintf(gpfOut, "%.2f %.2f scale\n", SQRT, SQRT);
    fprintf(gpfOut, "90 rotate\n");
    switch ( giPage % 2 ) {
    case 1:
      /* Odd page */
      fprintf(gpfOut, "0 -%d translate\n", giHeight);
      break;
    case 0:
      /* Even page */
      fprintf(gpfOut, "%d -%d translate\n", giWidth, giHeight);
      break;
    }
    break;
  case 0x12:
    /* Portrait Quartet */
    fprintf(gpfOut, "0.5 0.5 scale\n");
    switch ( giPage % 4 ) {
    case 1:
      fprintf(gpfOut, "0 %d translate\n", giHeight);
      break;
    case 2:
      fprintf(gpfOut, "%d %d translate\n", giWidth, giHeight);
      break;
    case 3:
      break;
    case 0:
      fprintf(gpfOut, "%d 0 translate\n", giWidth);
      break;
    }
    break;
  case 0x13:
    /* Portrait Quartet Vertical */
    fprintf(gpfOut, "0.5 0.5 scale\n");
    switch ( giPage % 4 ) {
    case 1:
      fprintf(gpfOut, "0 %d translate\n", giHeight);
      break;
    case 2:
      break;
    case 3:
      fprintf(gpfOut, "%d %d translate\n", giWidth, giHeight);
      break;
    case 0:
      fprintf(gpfOut, "%d 0 translate\n", giWidth);
      break;
    }
    break;
  }
  /* Box printing */
  if ( guBox ) {
    fprintf(gpfOut, "\ngsave\n");
    fprintf(gpfOut, "  1.0 setlinewidth \n");
    fprintf(gpfOut, "  newpath\n");
    fprintf(gpfOut, "  %.2f %.2f moveto\n", LEFT-5.0, BOTTOM-5.0);
    fprintf(gpfOut, "  %.2f %.2f lineto\n", LEFT-5.0, giHeight-TOP+15.0);
    fprintf(gpfOut, "  %.2f %.2f lineto\n", giWidth-RIGHT+5.0, giHeight-TOP+15.0);
    fprintf(gpfOut, "  %.2f %.2f lineto\n", giWidth-RIGHT+5.0, BOTTOM-5.0);
    fprintf(gpfOut, "  closepath\n");
    fprintf(gpfOut, "  %.2f %.2f moveto\n", LEFT-5.0, giHeight-TOP-5.0);
    fprintf(gpfOut, "  %.2f %.2f lineto\n", giWidth-RIGHT+5.0, giHeight-TOP-5.0);
    fprintf(gpfOut, "  stroke\n");
    fprintf(gpfOut, "grestore\n");
  }
  /* First Position */
  gfX = gfLeftMargin;
  gfY = (float)giHeight-gfTopMargin;
  /* Header */
  if ( guHeader ) {
    fprintf(gpfOut, "\n%% Header\n");
    /* Date*/
    if ( guKanjiCode == 0 ) {
      print_header(gsNowTime, "HEAD findfont 12 scalefont setfont", "HEAD-KANJI findfont 12 scalefont setfont", gfX, gfY, 0);
    }
    else {
      fprintf(gpfOut, "\n%.2f %.2f moveto\n", gfX, gfY);
      fprintf(gpfOut, "HEAD findfont 12 scalefont setfont\n");
      fprintf(gpfOut, "(%s) show\n", gsNowTime);
    }
    /* FileName or Title */
    if ( gsHeadStr[0] != '\0' ) {
      strcpy(sStr, gsHeadStr);
    }
    else {
      strcpy(sStr, sFileName);
    }
    if ( guKanjiCode == 0 ) {
      print_header(sStr, "HEAD-BOLD findfont 14 scalefont setfont", "HEAD-KANJI-BOLD findfont 14 scalefont setfont", (float)giWidth/2.0, gfY, 1);
    }
    else {
      fprintf(gpfOut, "HEAD-BOLD findfont 14 scalefont setfont\n");
      fprintf(gpfOut, "%.2f (%s) stringwidth pop sub 2 div %.2f moveto\n", (float)giWidth, sStr, gfY);
      fprintf(gpfOut, "(%s) show\n", sStr);
    }
    /* Page */
    sprintf(sStr, "%s %d", gsPageStr, giPage);
    if ( guKanjiCode == 0 ) {
      print_header(sStr, "HEAD findfont 12 scalefont setfont", "HEAD-KANJI findfont 12 scalefont setfont", (float)giWidth-gfRightMargin, gfY, 2);
    }
    else {
      fprintf(gpfOut, "HEAD findfont 12 scalefont setfont\n");
      fprintf(gpfOut, "%.2f (%s %d) stringwidth pop sub %.2f moveto\n", giWidth-gfRightMargin, gsPageStr, giPage, gfY);
      fprintf(gpfOut, "(%s %d) show\n\n", gsPageStr, giPage);
    }
    /* Move to first position */
    gfY -= gfFontSize * gfNlRate + 14.0;
    fprintf(gpfOut, "\n%.2f %.2f moveto\n", gfX, gfY);
  }
  else {
    fprintf(gpfOut, "\n%.2f %.2f moveto\n", gfX, gfY);
  }

  return;
}


/* Font */
void print_font(char *sFont) {
  if ( !strcmp(sFont, gsAsciiFont) ) {
    fprintf(gpfOut, "A-R  ");
  }
  else if ( !strcmp(sFont, gsKanjiFont) ) {
    fprintf(gpfOut, "K-R  ");
  }
  else if ( !strcmp(sFont, gsBoldItalicAsciiFont) ) {
    fprintf(gpfOut, "A-BI ");
  }
  else if ( !strcmp(sFont, gsBoldItalicKanjiFont) ) {
    fprintf(gpfOut, "K-BI ");
  }

  return;
}


/* Print Header */
void print_header(char *sStr, char *sAscii, char *sKanji, float fX, float fY, uchar uAlign) {
  uchar		*sEUC;

  /* Change to EUC code */
  sEUC = change2euc(sStr);
  if ( sEUC == NULL ) return;

  /* Check Align */
  switch ( uAlign ) {
  default:
  case 0:
    /* Left */
    fprintf(gpfOut, "%.2f %.2f moveto\n", fX, fY);
    print_header_string(sEUC, sAscii, sKanji);
    /* fprintf(gpfOut, "%s\n", sAscii); */
    /* fprintf(gpfOut, "(%s) show\n", sEUC); */
    break;
  case 1:
    /* Center */
    fprintf(gpfOut, "%.2f %.2f moveto\n", fX, fY);
    /* fprintf(gpfOut, "%s\n", sAscii); */
    /* fprintf(gpfOut, "%.2f (%s) stringwidth pop sub 2 div %.2f moveto\n", fX, sEUC, fY); */
    /* fprintf(gpfOut, "(%s) show\n", sEUC); */
    print_header_size(sEUC, sAscii, sKanji);
    fprintf(gpfOut, "2 div neg 0 rmoveto\n");
    print_header_string(sEUC, sAscii, sKanji);
    break;
  case 2:
    /* Right */
    fprintf(gpfOut, "%.2f %.2f moveto\n", fX, fY);
    /* fprintf(gpfOut, "%s\n", sAscii); */
    /* fprintf(gpfOut, "%.2f (%s) stringwidth pop sub %.2f moveto\n", fX, sEUC, fY); */
    /* fprintf(gpfOut, "(%s) show\n", sEUC); */
    print_header_size(sEUC, sAscii, sKanji);
    fprintf(gpfOut, "neg 0 rmoveto\n");
    print_header_string(sEUC, sAscii, sKanji);
    break;
  }

  free(sEUC);

  return;
}


/* Print header string size */
void print_header_size(uchar *sStr, char *sAscii, char *sKanji) {
  char		sTmp[STRMAX];
  uchar		uMode, uStart;
  int		i, j, iSize;

  iSize = strlen(sStr);
  uStart = 0;
  j = 0;
  sTmp[0] = '\0';
  uMode = 0;
  for ( i = 0 ; i < iSize ; i++ ) {
    if ( *(sStr+i) < 0x80 ) {
      if ( uMode == 1 && j > 0 ) {
	fprintf(gpfOut, "%s\n(", sKanji);
	for ( j = 0 ; sTmp[j] != '\0' ; j++ ) {
	  fprintf(gpfOut, "\\%03o", sTmp[j] & 0x7f);
	}
	if ( uStart == 0 ) {
	  fprintf(gpfOut, ") stringwidth pop\n");
	  uStart = 1;
	}
	else {
	  fprintf(gpfOut, ") stringwidth pop add\n");
	}
	j = 0;
	sTmp[0] = '\0';
      }
      sTmp[j++] = *(sStr+i);
      sTmp[j] = '\0';
      uMode = 0;
    }
    else {
      if ( uMode == 0 && j > 0 ) {
	if ( uStart == 0 ) {
	  fprintf(gpfOut, "%s\n(%s) stringwidth pop\n", sAscii, sTmp);
	  uStart = 1;
	}
	else {
	  fprintf(gpfOut, "%s\n(%s) stringwidth pop add\n", sAscii, sTmp);
	}
	j = 0;
	sTmp[0] = '\0';
      }
      sTmp[j++] = *(sStr+i++);
      sTmp[j++] = *(sStr+i);
      sTmp[j] = '\0';
      uMode = 1;
    }
  }
  if ( j > 0 ) {
    if ( uMode == 0 ) {
      if ( uStart == 0 ) {
	fprintf(gpfOut, "%s\n(%s) stringwidth pop\n", sAscii, sTmp);
	uStart = 1;
      }
      else {
	fprintf(gpfOut, "%s\n(%s) stringwidth pop add\n", sAscii, sTmp);
      }
    }
    else {
      fprintf(gpfOut, "%s\n(", sKanji);
      for ( j = 0 ; sTmp[j] != '\0' ; j++ ) {
	fprintf(gpfOut, "\\%03o", sTmp[j] & 0x7f);
      }
      if ( uStart == 0 ) {
	fprintf(gpfOut, ") stringwidth pop\n");
	uStart = 1;
      }
      else {
	fprintf(gpfOut, ") stringwidth pop add\n");
      }
    }
    j = 0;
    sTmp[0] = '\0';
  }

  return;
}


/* Print header string */
void print_header_string(uchar *sStr, char *sAscii, char *sKanji) {
  char		sTmp[STRMAX];
  uchar		uMode;
  int		i, j, iSize;

  iSize = strlen(sStr);
  j = 0;
  sTmp[0] = '\0';
  uMode = 0;
  for ( i = 0 ; i < iSize ; i++ ) {
    if ( *(sStr+i) < 0x80 ) {
      if ( uMode == 1 && j > 0 ) {
	fprintf(gpfOut, "%s\n(", sKanji);
	for ( j = 0 ; sTmp[j] != '\0' ; j++ ) {
	  fprintf(gpfOut, "\\%03o", sTmp[j] & 0x7f);
	}
	fprintf(gpfOut, ") show\n");
	j = 0;
	sTmp[0] = '\0';
      }
      sTmp[j++] = *(sStr+i);
      sTmp[j] = '\0';
      uMode = 0;
    }
    else {
      if ( uMode == 0 && j > 0 ) {
	fprintf(gpfOut, "%s\n(%s) show\n", sAscii, sTmp);
	j = 0;
	sTmp[0] = '\0';
      }
      sTmp[j++] = *(sStr+i++);
      sTmp[j++] = *(sStr+i);
      sTmp[j] = '\0';
      uMode = 1;
    }
  }
  if ( j > 0 ) {
    if ( uMode == 0 ) {
      fprintf(gpfOut, "%s\n(%s) show\n", sAscii, sTmp);
    }
    else {
      fprintf(gpfOut, "%s\n(", sKanji);
      for ( j = 0 ; sTmp[j] != '\0' ; j++ ) {
	fprintf(gpfOut, "\\%03o", sTmp[j] & 0x7f);
      }
      fprintf(gpfOut, ") show\n");
    }
    j = 0;
    sTmp[0] = '\0';
  }

  return;
}


/* Escape */
char *escape(char ch) {
  if ( ch == '(' || ch == ')' || ch == '<' || ch == '>' || ch == '\\' ) {
    sprintf(gsEscapedStr, "\\%c", ch);
  }
  else {
    sprintf(gsEscapedStr, "%c", ch);
  }

  return gsEscapedStr;
}


/* malloc string area */
char *setstring(char *sStr) {
  char	*sMalloc;

  if ( sStr == NULL || *sStr == '\0' ) return NULL;

  if ( ( sMalloc = (char *)malloc(sizeof(char)*(strlen(sStr)+1)) ) == NULL ) {
    if ( guJapanese ) {
      fprintf(stderr, "メモリが足りません。\n");
    }
    else {
      fprintf(stderr, "Memory overflow.\n");
    }
    exit(0);
  }

  strcpy(sMalloc, sStr);

  return sMalloc;
}


/* malloc string area2 */
char *setstringsize(int iSize) {
  char	*sMalloc;

  if ( iSize <= 0 ) return NULL;

  if ( ( sMalloc = (char *)malloc(sizeof(char)*iSize) ) == NULL ) {
    if ( guJapanese ) {
      fprintf(stderr, "メモリが足りません。\n");
    }
    else {
      fprintf(stderr, "Memory overflow.\n");
    }
    exit(0);
  }

  return sMalloc;
}


/* string realloc */
char *addstring(char *sOrigin, char *sStr) {
  char	*sRealloc;
  int	length;

  if ( sStr == NULL || *sStr == '\0' ) return sOrigin;

  if ( sOrigin == NULL ) return setstring(sStr);

  length = strlen(sOrigin) + strlen(sStr);

  if ( ( sRealloc = (char *)realloc(sOrigin, sizeof(char)*(length+1)) ) == NULL ) {
    if ( guJapanese ) {
      fprintf(stderr, "メモリが足りません。\n");
    }
    else {
      fprintf(stderr, "Memory overflow.\n");
    }
    exit(0);
  }

  strcat(sRealloc, sStr);

  return sRealloc;
}


/* Signal of Quit */
void quit(int sig) {
  signal(sig, SIG_IGN);

  exit(1);
}


/* Error and Help */
void help(char *sStr) {
  if ( sStr != NULL && *sStr != '\0' ) {
    fprintf(stderr, "\n%s\n", sStr);
  }

  fprintf(stderr, "\n'%s' Version %.2f (%s) ---  White Tools Presents.\n", gsCommand, VERSION/100.0, DATE);
  fprintf(stderr, "                Copyright (c) %s  %s\n\n", COPYRIGHTYEAR, CREATOR);
  if ( guJapanese ) {
    fprintf(stderr, "使い方 : %s\n", gsCommand);
    fprintf(stderr, "    [-l|-l2|-l4|-lv4|-p|-p2|-p4|-pv4] [-af ascii-font] [-abf ascii-bold]\n");
    fprintf(stderr, "    [-abif ascii-bolditalic] [-size fontsize] [-ls nlrate] [-line line]\n");
    fprintf(stderr, "    [-a4|-b4|-le] [-ohp] [-box] [-tm margin] [-bm margin] [-lm margin]\n");
    fprintf(stderr, "    [-rm margin][-nh] [-P printer] [-date string] [-head string]\n");
#ifdef	TEKTRO
    fprintf(stderr, "    [-page string] [-e] [-j] [-copy num] [-dup] [-h|--help] [-v|--version] \n");
    fprintf(stderr, "    [-fl] [-lib] [filenames...]\n\n");
#else
    fprintf(stderr, "    [-page string] [-e] [-j] [-h|--help] [-v|--version] [-fl] [-lib]\n");
    fprintf(stderr, "    [filenames...]\n\n");
#endif
    fprintf(stderr, "    -l                        : ランドスケープ\n");
    fprintf(stderr, "    -l2                       : ランドスケープ2面表示\n");
    fprintf(stderr, "    -l4                       : ランドスケープ4面表示\n");
    fprintf(stderr, "    -lv4                      : ランドスケープ4面表示縦順\n");
    fprintf(stderr, "    -p                        : ポートレイト\n");
    fprintf(stderr, "    -p2                       : ポートレイト2面表示 (デフォルト)\n");
    fprintf(stderr, "    -p4                       : ポートレイト4面表示\n");
    fprintf(stderr, "    -pv4                      : ポートレイト4面表示縦順\n");
    fprintf(stderr, "    -af ascii-font            : アスキーフォント   (%s)\n", gsAsciiFont);
    fprintf(stderr, "    -abf ascii-bold           : ボールドフォント   (%s)\n", gsBoldAsciiFont);
    fprintf(stderr, "    -abif ascii-bolditalic    : ボールドイタリック (%s)\n", gsBoldItalicAsciiFont);
    fprintf(stderr, "    -size fontsize            : フォントサイズ (%.1f)\n", FONTSIZE);
    fprintf(stderr, "    -afw magnification        : アスキー横倍率 (%.1f)\n", ASCIIWIDTH);
    fprintf(stderr, "    -afh magnification        : アスキー縦倍率 (%.1f)\n", ASCIIHEIGHT);
    fprintf(stderr, "    -kfw magnification        : 漢字横倍率 (%.1f)\n", KANJIWIDTH);
    fprintf(stderr, "    -kfh magnification        : 漢字縦倍率 (%.1f)\n", KANJIHEIGHT);
    fprintf(stderr, "    -ls nlrate                : 改行比率 (%.1f)\n", NLRATE);
    fprintf(stderr, "    -line line                : ページの最大行数 (%d)\n", MAXLINE);
    fprintf(stderr, "    -a4                       : A4サイズ印刷 (デフォルト)\n");
    fprintf(stderr, "    -b4                       : B4サイズ印刷\n");
    fprintf(stderr, "    -le                       : レターサイズ印刷\n");
    fprintf(stderr, "    -ohp                      : OHP用印刷 (フォントサイズ:%0.1f)\n", OHPFONTSIZE);
    fprintf(stderr, "    -box                      : 枠印刷 (looks like a2ps)\n");
    fprintf(stderr, "    -tm margin                : 上余白\n");
    fprintf(stderr, "    -bm margin                : 下余白\n");
    fprintf(stderr, "    -lm margin                : 左余白\n");
    fprintf(stderr, "    -rm margin                : 右余白\n");
    fprintf(stderr, "    -nh                       : ヘッダなし\n");
    fprintf(stderr, "    -Pprinter                 : プリンタセット\n");
    fprintf(stderr, "    -date string              : 日付文字列 (%s)\n", gsNowTime);
    fprintf(stderr, "    -head string              : ヘッダ文字列\n");
    fprintf(stderr, "    -page string              : ページ文字列 (デフォルト:Page)\n");
#ifdef	JIS_PS
    fprintf(stderr, "    -e                        : EUCフォントを利用して印刷\n");
    fprintf(stderr, "    -j                        : JISフォントを利用して印刷(デフォルト)\n");
#else
    fprintf(stderr, "    -e                        : EUCフォントを利用して印刷(デフォルト)\n");
    fprintf(stderr, "    -j                        : JISフォントを利用して印刷\n");
#endif
#ifdef	TEKTRO
    fprintf(stderr, "    -dup                      : 両面印刷\n");
    fprintf(stderr, "    -copy number              : 部数指定印刷 (1)\n");
#endif
    fprintf(stderr, "    -h | --help               : ヘルプの表示\n");
    fprintf(stderr, "    -v | --version            : バージョンの表示\n");
    fprintf(stderr, "    -fl                       : フォントリスト表示\n");
    fprintf(stderr, "    -lib                      : 各種環境表示\n\n");
  }
  else {
    fprintf(stderr, "Usage : %s\n", gsCommand);
    fprintf(stderr, "    [-l|-l2|-l4|-lv4|-p|-p2|-p4|-pv4] [-af ascii-font] [-abf ascii-bold]\n");
    fprintf(stderr, "    [-abif ascii-bolditalic] [-size fontsize] [-ls nlrate] [-line line]\n");
    fprintf(stderr, "    [-a4|-b4|-le] [-ohp] [-box] [-tm margin] [-bm margin] [-lm margin]\n");
    fprintf(stderr, "    [-rm margin] [-nh] [-P printer] [-date string] [-head string]\n");
#ifdef	TEKTRO
    fprintf(stderr, "    [-page string] [-e] [-j] [-copy num] [-dup] [-h|--help] [-v|--version]\n");
    fprintf(stderr, "    [-fl] [-lib] [filenames...]\n\n");
#else
    fprintf(stderr, "    [-page string] [-e] [-j] [-h|--help] [-v|--version] [-fl] [-lib]\n");
    fprintf(stderr, "    [filenames...]\n\n");
#endif
    fprintf(stderr, "    -l                        : landscape\n");
    fprintf(stderr, "    -l2                       : landscape 2 pages/paper\n");
    fprintf(stderr, "    -l4                       : landscape 4 pages/paper\n");
    fprintf(stderr, "    -lv4                      : landscape vertical 4 pages/paper\n");
    fprintf(stderr, "    -p                        : portrate\n");
    fprintf(stderr, "    -p2                       : portrate 2 pages/paper (default)\n");
    fprintf(stderr, "    -p4                       : portrate 4 pages/paper\n");
    fprintf(stderr, "    -pv4                      : portrate vertical 4 pages/paper\n");
    fprintf(stderr, "    -af ascii-font            : ASCII font (%s)\n", gsAsciiFont);
    fprintf(stderr, "    -abf ascii-bold           : ASCII bold font (%s)\n", gsBoldAsciiFont);
    fprintf(stderr, "    -abif ascii-bolditalic    : ASCII bold italic font (%s)\n", gsBoldItalicAsciiFont);
    fprintf(stderr, "    -size fontsize            : font size (%0.1f)\n", FONTSIZE);
    fprintf(stderr, "    -afw magnification        : ASCII width  (%.1f)\n", ASCIIWIDTH);
    fprintf(stderr, "    -afh magnification        : ASCII height (%.1f)\n", ASCIIHEIGHT);
    fprintf(stderr, "    -kfw magnification        : ASCII width  (%.1f)\n", KANJIWIDTH);
    fprintf(stderr, "    -kfh magnification        : ASCII height (%.1f)\n", KANJIHEIGHT);
    fprintf(stderr, "    -ls nlrate                : space ratio between lines (%0.1f)\n", NLRATE);
    fprintf(stderr, "    -line line                : max lines (%d)\n", MAXLINE);
    fprintf(stderr, "    -a4                       : A4 paper (default)\n");
    fprintf(stderr, "    -b4                       : B4 paper\n");
    fprintf(stderr, "    -le                       : letter paper\n");
    fprintf(stderr, "    -ohp                      : OHP printing (font size:%0.1f)\n", OHPFONTSIZE);
    fprintf(stderr, "    -box                      : print page frame-line (looks like a2ps)\n");
    fprintf(stderr, "    -tm margin                : top    margin\n");
    fprintf(stderr, "    -bm margin                : bottom margin\n");
    fprintf(stderr, "    -lm margin                : left   margin\n");
    fprintf(stderr, "    -rm margin                : right  margin\n");
    fprintf(stderr, "    -nh                       : not print headers\n");
    fprintf(stderr, "    -Pprinter                 : printer\n");
    fprintf(stderr, "    -date string              : date string (%s)\n", gsNowTime);
    fprintf(stderr, "    -head string              : header string\n");
    fprintf(stderr, "    -page string              : page string (Page)\n");
#ifdef	JIS_PS
    fprintf(stderr, "    -e                        : print with EUC postscript font\n");
    fprintf(stderr, "    -j                        : print with JIS postscript font(default)\n");
#else
    fprintf(stderr, "    -e                        : print with EUC postscript font(default)\n");
    fprintf(stderr, "    -j                        : print with JIS postscript font\n");
#endif
#ifdef	TEKTRO
    fprintf(stderr, "    -dup                      : double side printing\n");
    fprintf(stderr, "    -copy number              : number of papers (1)\n");
#endif
    fprintf(stderr, "    -h | --help               : show help\n");
    fprintf(stderr, "    -v | --version            : show version\n");
    fprintf(stderr, "    -fl                       : show font list\n");
    fprintf(stderr, "    -lib                      : show library\n\n");
  }

  exit(1);
}


/*=============================================================================
			end
=============================================================================*/
